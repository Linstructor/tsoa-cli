import { findNecessaryFiles, isFileAvailable, updateRouter } from '../src/FileGenerator';
import { Args } from '../src/model/Args';
import * as path from 'path';
import * as fs from 'fs';
import { NecessaryFiles } from '../src/model/NecessaryFiles';

describe('FileGenerator', function() {
    it('should find necessary files', () => {
        const args = <Args>{srcPath: path.resolve(__dirname, 'template', 'src')};
        const filesPath = findNecessaryFiles(args);
        expect(filesPath.controllerFolder).not.toBeUndefined();
        expect(filesPath.serviceFolder).not.toBeUndefined();
        expect(filesPath.modelFolder).not.toBeUndefined();
        expect(filesPath.router).not.toBeUndefined();
        expect(filesPath.modelFile).toBeUndefined();
    });

    it('should find necessary files with model file', () => {
        fs.writeFileSync(path.resolve(__dirname, 'template', 'src', 'models', 'Test.ts'), '');
        const args = <Args>{
            srcPath: path.resolve(__dirname, 'template', 'src'),
            existingModelName: 'Test'
        };
        const filesPath = findNecessaryFiles(args);
        expect(filesPath.controllerFolder).not.toBeUndefined();
        expect(filesPath.serviceFolder).not.toBeUndefined();
        expect(filesPath.modelFolder).not.toBeUndefined();
        expect(filesPath.router).not.toBeUndefined();
        expect(filesPath.modelFile).not.toBeUndefined();
    });

    it('should verify if files exist', () => {
        const args = <Args>{
            srcPath: path.resolve(__dirname, 'template', 'src'),
            routeName: 'AB CD EF'
        };
        const filesPath = findNecessaryFiles(args);
        isFileAvailable(args, filesPath);
    });

    it('should verify if files exist with model', () => {
        fs.writeFileSync(path.resolve(__dirname, 'template', 'src', 'models', 'Test.ts'), '');
        const args = <Args>{
            srcPath: path.resolve(__dirname, 'template', 'src'),
            routeName: 'AB CD EF',
            existingModelName: 'Test'
        };
        const filesPath = findNecessaryFiles(args);
        expect(isFileAvailable(args, filesPath)).toBeTruthy();
    });

    it('should modify router', () => {
        updateRouter(<NecessaryFiles>{router: path.resolve(__dirname, 'template', 'src', 'router.ts')}, 'controllerName', 'serviceName');
    });
});