import { getArgs } from '../src/ArgsReader';

describe('ArgsReader', function() {
    it('should return transformed args', () => {
        const processArgs: string[] = [
            process.argv0,
            process.argv[1],
            'create:route',
            '--actions',
            'CRU',
            '-h',
            '--name',
            'RouteName',
            '-g'
        ];
        const result = getArgs(processArgs);
        expect(result).toHaveProperty('routeName', 'RouteName');
        expect(result).toHaveProperty('endpointName', 'RouteName');
        expect(result).toHaveProperty('shouldCreateModel', true);
        expect(result).toHaveProperty('shouldUseExistingModel', false);
        expect(result).toHaveProperty('existingModelName', undefined);
        expect(result).toHaveProperty('paginate', false);
        expect(result).toHaveProperty('hidden', true);
        expect(result).toHaveProperty('actions', 'CRU'.split(''));
        expect(result).toHaveProperty('command', 'generate');
        expect(result).toHaveProperty('srcPath', process.cwd());
    });
});