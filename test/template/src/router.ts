import { Router } from ‘express’;
import { type } from ‘os’;
import * as winston from ‘winston’;
// @ts-ignore
import { RegisterRoutes } from ‘../_gen/routes/routes’;
import {
    Code,
    ErrorResponse,
    HttpRequest,
    HttpResponse,
    HttpResponseError,
    LambdaProxyCallback,
    LambdaProxyEvent, Type
} from ‘./framework’;
​
// reference for dynamic generation
import { buildProviderModule } from ‘inversify-binding-decorators’;
​
// Import services
import ‘./services/AuthService’;
import ‘./services/CityService’;
import ‘./services/DealtermService’;
import ‘./services/JourneyService’;
import ‘./services/ShowcaseService’;
import ‘./services/TicketService’;
import ‘./services/UserService’;
​
// Import controllers
import ‘./controllers/AuthController’;
import ‘./controllers/CityController’;
import ‘./controllers/DealtermController’;
import ‘./controllers/JourneyController’;
import ‘./controllers/ShowcaseController’;
import ‘./controllers/UserController’;
​
import { iocContainer } from ‘Ioc’;
​
// must run buildProviderModule after all imports that use binding decorators have been imported
iocContainer.load(buildProviderModule());
​
const swaggerJson = require(‘../_gen/swagger/swagger.json’);
​
winston.configure({
    exitOnError: false,
    transports: [
        new winston.transports.Console()
    ]
});
​
const router = Router();
​
router.get(‘/v1/swagger.json’, (req, res) => {
    res.json(swaggerJson);
});
​
type middlewareExec = ((request: HttpRequest, response: HttpResponse, next: any) => void);
​
function methodHandler(method: string) {
    return (route: string, ...routeExecs: middlewareExec[]) => {
        router[method](route, (req, res) => {
            winston.info(`Found route ${route}`);
​
            const runNext = (runExecs: middlewareExec[]) => {
                const curExec: middlewareExec = runExecs[0];
​
                curExec(req, res, (err) => {
                    if (err) {
                        if (err instanceof HttpResponseError) {
                            res.status(err.statusCode).json(
                                new ErrorResponse(err.payload.code, err.payload.type, err.message, err.payload.param)
                            );
                            return;
                        }
​
                        winston.error(`Unhandled Exception: ${JSON.stringify(err.stack || err)}`);
​
                        res.status(500).json(
                            new ErrorResponse(
                                Type.apiError,
                                Code.requestFailure,
                            ‘There was an error processing your request.’));
                    } else if (runExecs.length > 1) {
                        runNext(runExecs.slice(1));
                    }
                });
            };
​
            runNext(routeExecs);
        });
    };
}
​
const mockApp = {
    delete: methodHandler(‘delete’),
get: methodHandler(‘get’),
patch: methodHandler(‘patch’),
post: methodHandler(‘post’),
put: methodHandler(‘put’)
};
​
RegisterRoutes(mockApp as any);
​
require(‘./database/rds/RdsService’);
​
export function handler(event: LambdaProxyEvent, context, callback: LambdaProxyCallback) {
    context.callbackWaitsForEmptyEventLoop = false;
    winston.info(`handling ${event.httpMethod} ${event.path}`);
    const response = new HttpResponse((error, res) => {
        if (error) {
            return context.fail(error);
        }
        context.succeed(res);
    });
​
    if (event.httpMethod.toLowerCase() === ‘options’) {
        response.status(200).end();
    } else {
        (router as any).handle(new HttpRequest(event), response, (err) => {
            if (!err) {
                winston.info(`404 for ${event.httpMethod} ${event.path}`);
                response.status(404).json(new ErrorResponse(Code.notFound, Type.requestError, ‘Not Found’));
            }
            winston.info(`500 for ${event.httpMethod} ${event.path}`);
            response.status(500).json(new ErrorResponse(Code.requestFailure, Type.apiError, process.env.NODE_ENV === ‘development’ ?
                err : ‘An error has occured.’));
        });
    }
// });
    import * from './controllers/controllerName'
    import * from './services/serviceName'
    
import * from './controllers/controllerName';
import * from './services/serviceName';
    
import * from './controllers/RunTestController';
import * from './services/RunTestService';
    