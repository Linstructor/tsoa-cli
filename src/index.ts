#!/usr/bin/env node

import { getArgs, isValidArgs } from './ArgsReader';
import {
    createController,
    createModel,
    createService,
    findNecessaryFiles,
    formatFile,
    isFileAvailable,
    updateRouter,
} from './FileGenerator';
import camelcase from 'camelcase';
import ora from 'ora';
import chalk from 'chalk';

const spinner = ora('💡 Loading arguments').start();
const args = getArgs(process.argv);
if (!isValidArgs(args)) {
    spinner.fail('⛔️ Argument error');
    spinner.stop();
    process.exit(1);
}
spinner.succeed();
spinner.start('🗂 Searching files');

const filesPaths = findNecessaryFiles(args);

try {
    isFileAvailable(args, filesPaths);
} catch (e) {
    spinner.fail('⛔  Files error');
    spinner.stop();
    console.error(chalk.red(e.message));
    process.exit(1);
}
spinner.succeed();

const globalName =
    camelcase(args.routeName)
        .charAt(0)
        .toUpperCase() + camelcase(args.routeName).substr(1);
const modelName = args.shouldUseExistingModel ? args.existingModelName : globalName;
const serviceName = globalName + 'Service';
const controllerName = globalName + 'Controller';

if (!args.shouldUseExistingModel) {
    createModel(modelName, filesPaths.modelFolder);
    formatFile(modelName, filesPaths.modelFolder, filesPaths.tsconfigPath, filesPaths.tslintPath);
}

spinner.start('🔩 Creating service');
createService(args.actions, args.paginate, modelName, serviceName, filesPaths.serviceFolder);
formatFile(serviceName, filesPaths.serviceFolder, filesPaths.tsconfigPath, filesPaths.tslintPath);
spinner.succeed();
spinner.start('📡 Creating controller');
createController(
    args.actions,
    args.paginate,
    modelName,
    serviceName,
    controllerName,
    filesPaths.controllerFolder,
    args.routeName,
    args.endpointName,
    args.security
);
formatFile(controllerName, filesPaths.controllerFolder, filesPaths.tsconfigPath, filesPaths.tslintPath);
spinner.succeed();
spinner.start('🚀 Updating router');
updateRouter(filesPaths, controllerName, serviceName);
spinner.succeed();
spinner.stop();
