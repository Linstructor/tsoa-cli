import { Args } from './model/Args';
import args from 'args';
import pluralize from 'pluralize';
import chalk from 'chalk';

export const getArgs = (processArgs: string[]): Args => {
    const options = [
        { name: 'name', description: 'Route name' },
        { name: 'endpoint', description: 'Endpoints name', init: (content: string) => pluralize(content.toString()) },
        { name: 'hidden', description: 'Hide endpoints' },
        { name: 'security', description: 'Security middleware name' },
        // { name: 'model', description: 'Use existing model (name)' },
        { name: 'src', description: 'Src folder path', defaultValue: process.cwd() },
        {
            name: 'generate:route',
            description: 'Generate routes',
            init: (content: boolean) => (content ? 'generate' : undefined),
        },
        { name: 'paginate', description: 'Enable pagination on getAll' },
        {
            name: 'actions',
            description: 'Action to create: CRUD',
            defaultValue: 'CRUD',
            init: (content: string) => content.split(''),
        },
    ];

    args.options(options);
    args.example('tsoa-cli --generate:route --name User --actions CRUD', 'Create CRUD for User');

    // @ts-ignore
    const flags = args.parse(processArgs, { name: 'tsoa-cli' });

    return {
        actions: flags.actions,
        existingModelName: flags.model,
        hidden: flags.hidden,
        routeName: flags.name,
        paginate: !!flags.paginate,
        endpointName: flags.endpoint ? flags.endpoint : flags.name,
        shouldCreateModel: !flags.model,
        shouldUseExistingModel: !!flags.model,
        srcPath: flags.src,
        command: flags.g,
        security: flags.s,
    };
};

export const isValidArgs = (params: Args) => {
    let error = false;
    if (!params.routeName) {
        console.error(chalk.red('\nNo route name passed'));
        error = true;
    }
    if (params.actions.length === 0) {
        console.log(chalk.red('\nNo actions passed'));
        error = true;
    }
    if (!params.command) {
        console.log(chalk.red('\nNo command passed'));
        error = true;
    }
    if (params.security && typeof params.security === 'boolean') {
        console.log(chalk.red('\nInvalid middleware name passed'));
        error = true;
    }

    if (error) args.showHelp();
    return !error;
};
