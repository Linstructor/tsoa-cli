import { NecessaryFiles } from './model/NecessaryFiles';
import * as path from 'path';
import * as fs from 'fs';
import { Args } from './model/Args';
import camelcase from 'camelcase';
import * as controllerTemplate from './fileTemplate/Controller';
import * as serviceTemplate from './fileTemplate/Service';
import * as tsfmt from 'typescript-formatter';

export const findNecessaryFiles = (args: Args): NecessaryFiles => {
    const controllersFolder = path.resolve(args.srcPath, 'controllers');
    const servicesFolder = path.resolve(args.srcPath, 'services');
    const modelsFolder = path.resolve(args.srcPath, 'models');
    const routerPath = path.resolve(args.srcPath, 'router.ts');
    const tsconfigPath = path.resolve(args.srcPath, '..', 'tsconfig.json');
    const tslintPath = path.resolve(args.srcPath, '..', 'tslint.json');

    let modelPath;

    if (!fs.existsSync(controllersFolder)) throw new Error('Controllers folder doesnt exists');
    if (!fs.existsSync(servicesFolder)) throw new Error('Services folder doesnt exists');
    if (!fs.existsSync(routerPath)) throw new Error('Route file doesnt exists');
    if (!fs.existsSync(modelsFolder)) throw new Error('Models folder doesnt exists');
    if (!fs.existsSync(tsconfigPath)) throw new Error('Tsconfig file doesnt exists');
    if (!fs.existsSync(tslintPath)) throw new Error('Tslint file doesnt exists');

    if (args && args.existingModelName) {
        modelPath = path.resolve(args.srcPath, 'models', `${args.existingModelName}.ts`);
        if (!fs.existsSync(modelPath)) throw new Error('Model not found');
    }

    return {
        controllerFolder: controllersFolder,
        modelFile: modelPath,
        modelFolder: modelsFolder,
        router: routerPath,
        serviceFolder: servicesFolder,
        tsconfigPath: tsconfigPath,
        tslintPath: tslintPath,
    };
};

export const isFileAvailable = (args: Args, filesPath: NecessaryFiles) => {
    if (fs.existsSync(path.resolve(filesPath.serviceFolder, `${camelcase(args.routeName)}Service.ts`)))
        throw new Error(`${args.routeName} service already exists`);
    if (fs.existsSync(path.resolve(filesPath.controllerFolder, `${camelcase(args.routeName)}Controllers.ts`)))
        throw new Error(`${args.routeName} controller already exists`);
    if (args.shouldUseExistingModel) {
        if (!fs.existsSync(path.resolve(filesPath.controllerFolder, `${args.existingModelName}.ts`)))
            throw new Error(`${args.existingModelName} model doesnt exists`);
    }
    return true;
};

export const writeFile = (data: string, fileName: string, destination: string) => {
    return fs.writeFileSync(path.resolve(destination, `${fileName}.ts`), data);
};

export const generateStringFile = (template: FileTemplate, data: TemplateReplaceData[]) => {
    let string = fs.readFileSync(path.resolve(__dirname, 'fileTemplate', template.toString())).toString();
    data.forEach((entry) => (string = string.replace(entry.regex, entry.value)));
    return string;
};

export const generateService = (serviceName: string, modelName: string, content: string): string => {
    return generateStringFile(FileTemplate.SERVICE, [
        { regex: /%model_name%/g, value: modelName },
        { regex: /%service_name%/g, value: serviceName },
        { regex: /%content%/g, value: content },
    ]);
};

export const generateController = (
    controllerName: string,
    modelName: string,
    routeName: string,
    endpointName: string,
    serviceName: string,
    content: string,
) => {
    return generateStringFile(FileTemplate.CONTROLLER, [
        { regex: /%model_name%/g, value: modelName },
        { regex: /%route_name%/g, value: routeName },
        { regex: /%endpoint_name%/g, value: endpointName },
        { regex: /%controller_name%/g, value: controllerName },
        { regex: /%service_name%/g, value: serviceName },
        { regex: /%content%/g, value: content },
    ]);
};

export const generateModel = (modelName: string) => {
    return generateStringFile(FileTemplate.MODEL, [{ regex: /%model_name%/g, value: modelName }]);
};

export const updateRouter = (filesPaths: NecessaryFiles, controllerName: string, serviceName: string) => {
    let routerString = fs.readFileSync(filesPaths.router).toString('utf8');
    routerString = `${routerString}
import * from \'./controllers/${controllerName}\';
import * from \'./services/${serviceName}\';
    `;
    fs.writeFileSync(filesPaths.router, routerString);
};

export const contentCreator = (actions: string[], pagination: boolean, modelName: string, template: any, security?: string): string => {
    return actions
        .map((action) => {
            if (action === 'C') return template.create(modelName, security);
            if (action === 'R') return template.getAll(pagination, modelName, security) + template.getOne(modelName, security);
            if (action === 'U') return template.update(modelName, security);
            if (action === 'D') return template.remove(security);
        })
        .join('');
};

export const createService = (
    actions: string[],
    pagination: boolean,
    modelName: string,
    serviceName: string,
    servicePath: string,
) => {
    const content = contentCreator(actions, pagination, modelName, serviceTemplate);
    const data = generateService(serviceName, modelName, content);
    writeFile(data, serviceName, servicePath);
};

export const createController = (
    actions: string[],
    pagination: boolean,
    modelName: string,
    serviceName: string,
    controllerName: string,
    controllersPath: string,
    routeName: string,
    endpointName: string,
    security?: string
) => {
    const content = contentCreator(actions, pagination, modelName, controllerTemplate, security);
    const data = generateController(controllerName, modelName, routeName, endpointName, serviceName, content);
    writeFile(data, controllerName, controllersPath);
};

export const createModel = (modelName: string, modelsPath: string) => {
    const data = generateModel(modelName);
    writeFile(data, modelName, modelsPath);
};

export const formatFile = (filePath: string, fileName: string, tsconfigPath: string, tslintPath: string) => {
    tsfmt.processFiles(
        [path.resolve(filePath, fileName + '.ts')],
        //@ts-ignore
        {
            replace: true,
            tsconfig: true,
            tslint: true,
            tsconfigFile: tsconfigPath,
            tslintFile: tslintPath,
        },
    );
};

export enum FileTemplate {
    MODEL = 'model.txt',
    SERVICE = 'service.txt',
    CONTROLLER = 'controller.txt',
}

interface TemplateReplaceData {
    regex: RegExp;
    value: string;
}
