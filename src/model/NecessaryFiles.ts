export interface NecessaryFiles {
    router: string;
    controllerFolder: string;
    serviceFolder: string;
    modelFolder: string;
    modelFile?: string;
    tsconfigPath: string;
    tslintPath: string;
}
