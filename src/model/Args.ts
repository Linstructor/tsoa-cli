export interface Args {
    routeName: string;
    endpointName: string;
    shouldCreateModel: boolean;
    shouldUseExistingModel: boolean;
    existingModelName: string;
    paginate: boolean;
    hidden: boolean;
    actions: string[];
    command: string;
    srcPath: string;
    security: string;
}
