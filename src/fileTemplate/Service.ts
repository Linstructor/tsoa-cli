export const getAll = (pagination: boolean, modelName: string) => `
    public getAll(${pagination ? 'limit: number, page: number' : ''}): Promise<${
    !pagination ? modelName : `{data: ${modelName}[], count: number}`
}> {
        return new Promise<${modelName}>(async (resolve, reject) => {
        });
    }
`;

export const getOne = (modelName: string) => `
    public getOne(id: number): Promise<${modelName}> {
        return new Promise<${modelName}>(async (resolve, reject) => {
        });
    }
`;

export const create = (modelName: string) => `
    public create(payload: ${modelName}Request): Promise<${modelName}> {
        return new Promise<${modelName}>(async (resolve, reject) => {
        });
    }
`;

export const update = (modelName: string) => `
    public update(id: number, payload: ${modelName}Request): Promise<${modelName}> {
        return new Promise<${modelName}>(async (resolve, reject) => {
        });
    }
`;

export const remove = () => `
    public remove(id: number): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
        });
    }
`;
