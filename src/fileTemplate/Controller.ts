export const getAll = (pagination: boolean, modelName: string, security: string) =>
    `    @Get()
    ${security ? `@Security('${security}')` : ''}
    public getAll(${
        pagination ? "\n\t\t@Query('limit') limit = 30,\n" + "\t\t@Query('page') page = 1\n\t" : ''
    }): Promise<${!pagination ? modelName : `{data: ${modelName}[], count: number}`}> {
        return new Promise<any>((resolve, reject) => {
            this.service.getAll(${pagination ? 'limit, page' : ''})
                .then((data) => resolve(data))
                .catch((e) => reject(e));
        });
    }
`;

export const getOne = (modelName: string, security: string) =>
    `    @Get('{id}')
    ${security ? `@Security('${security}')` : ''}
    public getOne(id: number): Promise<${modelName}> {
        return new Promise<${modelName}>((resolve, reject) => {
            this.service.getOne(id)
                .then((data) => resolve(data))
                .catch((e) => reject(e));
        });
    }
`;

export const create = (modelName: string, security: string) =>
    `    @Post()
    ${security ? `@Security('${security}')` : ''}
    public create(@Body() body: ${modelName}Request): Promise<${modelName}> {
        return new Promise<${modelName}>((resolve, reject) => {
            this.service.create(body)
                .then((data) => resolve(data))
                .catch((e) => reject(e));
        });
    }
`;

export const update = (modelName: string, security: string) =>
    `    @Put('{id}')
    ${security ? `@Security('${security}')` : ''}
    public update(id: number, @Body() body: ${modelName}Request): Promise<${modelName}> {
        return new Promise<${modelName}>((resolve, reject) => {
            this.service.update(id, body)
                .then((data) => resolve(data))
                .catch((e) => reject(e));
        });
    }
`;

export const remove = (security: string) =>
    `    @Delete('{id}')
    ${security ? `@Security('${security}')` : ''}
    public remove(id: number): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            this.service.remove(id)
                .then(() => resolve())
                .catch((e) => reject(e));
        });
    }
`;
