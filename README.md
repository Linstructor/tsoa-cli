#tsoa-cli

Cli to automate simple task in tsoa architecture.

### Install
```
$ npm add tsoa-cli
$ yarn add tsoa-cli
```

**Tsoa architecture:**
```bash
basedir
|
|——src
    |———— controllers
    |———— services
    |———— models
    |——tsconfig.json
    |——tslint.json

```

## Action supported
Only one command are currently supported:
- `--generate:route` generate new route 

## Options
- **\*Actions**  `--actions, -a`: Define action implemented in the route (CRUD)
- **\*Name**     `--name, -n`: Name of the route
- Enpoint   `--endpoint, -e`: Name of the resource endpoint (name camelcased by default)
- Hidden    `--hidden, -h`: Hide endpoints
- Src       `--src, -s`: Tsoa src folder (execution folder by default)
- Paginate  `--paginate, -p`: Enable pagination
- Security  `--security, -s`: Set the security middleware name

_* Argument required_

##Examples
```bash
// Generate a route User with Create Read Update actions
$ tsoa-cli --generate:route --name User --actions CRU
```